%%%-------------------------------------------------------------------
%%% @author sewerin
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 27. May 2019 21:41
%%%-------------------------------------------------------------------
-module(chat).
-author("sewerin").
-behaviour(gen_server).

%% API
-export([init/1, handle_call/3, handle_cast/2]).
-export([send/1, send/2, register/1, register/2, start/0]).
-export([testReceive/0, list/0]).

testReceive() ->
  receive
    {message, Name, {Hour, Minute, Second}, Text} -> io:format("<~p:~p:~p>~s: ~s~n", [Hour, Minute, Second, Name, Text]);
    Messege -> Messege
  after 0 -> noMessege
  end.

send(Text) ->
  gen_server:call({global, ?MODULE}, {send, self(), Text}).

send(Pid, Text) ->
  gen_server:call({global, ?MODULE}, {send, Pid, Text}).

list() ->
  gen_server:call({global, ?MODULE}, list).

register(Name) ->
  gen_server:call({global, ?MODULE}, {register, self(), Name}).

register(Pid, Name) ->
  gen_server:call({global, ?MODULE}, {register, Pid, Name}).

start() ->
  gen_server:start_link({global, ?MODULE}, ?MODULE, #{}, []).

init(Args) ->
  {ok, Args}.

handle_call({register, Pid, Name}, _From, State) ->
  case maps:is_key(Pid, State) of
    true -> {reply, alreadyRegistered, State};
    false -> {reply, ok, maps:put(Pid, Name, State)}
  end;
handle_call(list, _From, State) ->
  {reply, State, State};
handle_call({send, Pid, Text}, _From, State) ->
  case maps:get(Pid, State) of
    {badkey, _} -> {reply, unregistered, State};
    Name ->
      {_Date, Time} = calendar:universal_time(),
      sendToAll(maps:keys(State), Name, Time, Text),
      {reply, ok, State}
  end.


handle_cast(_, _) ->
  erlang:error("No").
%%  {_Date, Time} = calendar:universal_time(),
%%  sendToAll(maps, Text),
%%  {noreply, State}.

sendToAll([], _, _, _) -> ok;
sendToAll([H | T], Name, Time, Text) ->
  H ! {message, Name, Time, Text},
  sendToAll(T, Name, Time, Text).
