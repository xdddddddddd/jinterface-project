import com.ericsson.otp.erlang.*;

import java.util.Scanner;

public class Application {

    public void start() throws Exception {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter node name: ");
        String nodeName = input.nextLine();
        String cookie = "chat";

        OtpPeer otpPeer = new OtpPeer("server");
        OtpSelf otpSelf = new OtpSelf(nodeName, cookie);
        OtpConnection connection = otpSelf.connect(otpPeer);

        boolean regUndone = true;
        while(regUndone) {
            OtpErlangList msg = new OtpErlangList(new OtpErlangObject[] {otpSelf.pid(), new OtpErlangString(nodeName)});
            connection.sendRPC("chat", "register", msg);
            OtpErlangObject answerRegister = connection.receiveRPC();

            if (!answerRegister.equals(new OtpErlangAtom("ok"))) {
                if (answerRegister.equals(new OtpErlangAtom("alreadyRegistered"))) {
                    System.out.println("Name already registered");
                    System.out.print("Enter name: ");
                    nodeName = input.nextLine();
                    otpSelf = new OtpSelf(nodeName, cookie);
                    connection = otpSelf.connect(otpPeer);
                } else {
                    throw new Exception("Could not register to server");
                }
            }else{
                regUndone = false;
            }
        }

        OtpConnection finalConnection = connection;
        Thread sender = new Thread(() -> {
            while(true) {
                System.out.print("Enter message: ");
                String messageText = input.nextLine();
                try{
                    finalConnection.sendRPC("chat", "send", new OtpErlangList( new OtpErlangObject[] { finalConnection.self().pid(), new OtpErlangString(messageText)}));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        OtpConnection finalConnection1 = connection;
        Thread receiver = new Thread(() ->{
            while(true){
                try {
                    OtpErlangObject receive = finalConnection1.receive();
                    if(receive instanceof OtpErlangTuple) {
                        OtpErlangTuple receiveTuple = (OtpErlangTuple) receive;
                        if (receiveTuple.arity() == 2) {
                            if (receiveTuple.elementAt(0).equals(new OtpErlangAtom("rex"))) {
                                if (!receiveTuple.elementAt(1).equals((new OtpErlangAtom("ok")))) {
                                    System.out.println("Woof woof!");
                                    System.out.println("Rex is not ok");
                                    System.out.println(receiveTuple);
                                }
                            }
                        } else if(receiveTuple.arity() == 4) {
                            if(receiveTuple.elementAt(0).equals(new OtpErlangAtom("message"))) {
                                System.out.print(receiveTuple.elementAt(2) + " ");
                                System.out.print(receiveTuple.elementAt(1) + ": ");
                                System.out.println(receiveTuple.elementAt(3));
                            } else {
                                System.out.println("What is receive?");
                                System.out.println(receive);
                            }
                        } else {
                            System.out.println("What is receive?");
                            System.out.println(receive);
                        }
                    } else {
                        System.out.println("What is receive?");
                        System.out.println(receive);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        receiver.start();
        sender.start();
    }
}